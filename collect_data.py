import requests
import os
import geopandas as gpd
import pandas as pd
import json

if not os.path.exists("data"):
    os.mkdir("data")


def collect(y, m, d, y2, m2, d2):
    date_range = f"{y}{m:02d}{d:02d} - {y2}{m2:02d}{d2:02d}"
    url = f"https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime={y}-{m:02d}-{d:02d}&endtime={y2}-{m2:02d}-{d2:02d}"
    print(url)
    response = requests.get(url)

    if response.status_code != 200:
        print(date_range, " - Statuscode not OK ", response.status_code)
        return gpd.GeoDataFrame()
    features = json.loads(response.content)["features"]
    filtered = list(filter(lambda x: type(None) not in [type(x["geometry"]["coordinates"][i]) for i in
                                                 range(len(x["geometry"]["coordinates"]))],
                    features))
    if len(features) != len(filtered):
        print("{} corrupted entries", len(features) - len(filtered))
    return gpd.GeoDataFrame.from_features(filtered)

for y in range(2002, 2023):
    fname = f"data/quakes{y}.geojson"
    df = gpd.GeoDataFrame()
    for m in range(1, 13):
        m2 = m + 1 if m < 12 else 1
        y2 = y if m2 > 1 else y + 1
        df = gpd.GeoDataFrame( pd.concat([
            df,
            collect(y, m, 1, y, m, 15),
            collect(y, m, 15, y2, m2, 1)
            ], ignore_index=True)
        )
    with open(fname, "w") as f:
        f.write(df.to_json())
